﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    enum Kategori
    {
        A,
        B,
        C,
        D,
    }

    public class Kitap
    {
        public int KitapID { get; set; }

        [Display(Name = "Kitap Adı")]
        public string KitapAd { get; set; }
        
        [DisplayFormat(DataFormatString = "{ 0:dd:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [Display(Name = "Yayın Tarihi")]
        public DateTime YayinTarihi { get; set; }

        public int SayfaSayisi { get; set; }


        public int Tiklanma { get; set; }

        public int YayinEviID { get; set; }

        public int YazarID { get; set; }

       // public string Kategori { get; set; }

        //public string GenelBilgi { get; set; }

        public virtual YayinEvi YayinEvi { get; set; }

        public virtual Yazar Yazar { get; set; }

    }
}