﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Yorum
    {
        public int YorumID { get; set; }

        public string YorumMetni { get; set; }

        public int KitapID { get; set; }

        //public int KullaniciID { get; set; }

        public virtual Kitap Kitap { get; set; }

        //public virtual Kullanici Kullanici { get; set; }

    }
}