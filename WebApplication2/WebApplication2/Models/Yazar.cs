﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication2.Models
{
    public class Yazar
    {
        public int YazarID { get; set; }

        public string YazarAd { get; set; }

        public string YazarSoyad { get; set; }

        public DateTime DogumTarihi { get; set; }

        public string YazarHakkinda { get; set; }

        //Resim eklemedin
    }
}