namespace WebApplication2.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Kitap",
                c => new
                    {
                        KitapID = c.Int(nullable: false, identity: true),
                        KitapAd = c.String(),
                        YayinTarihi = c.DateTime(nullable: false),
                        SayfaSayisi = c.Int(nullable: false),
                        Tiklanma = c.Int(nullable: false),
                        YayinEviID = c.Int(nullable: false),
                        YazarID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.KitapID)
                .ForeignKey("dbo.YayinEvi", t => t.YayinEviID, cascadeDelete: true)
                .ForeignKey("dbo.Yazar", t => t.YazarID, cascadeDelete: true)
                .Index(t => t.YayinEviID)
                .Index(t => t.YazarID);
            
            CreateTable(
                "dbo.YayinEvi",
                c => new
                    {
                        YayinEviID = c.Int(nullable: false, identity: true),
                        YayinEviAdi = c.String(),
                        YayinEviHakkinda = c.String(),
                    })
                .PrimaryKey(t => t.YayinEviID);
            
            CreateTable(
                "dbo.Yazar",
                c => new
                    {
                        YazarID = c.Int(nullable: false, identity: true),
                        YazarAd = c.String(),
                        YazarSoyad = c.String(),
                        DogumTarihi = c.DateTime(nullable: false),
                        YazarHakkinda = c.String(),
                    })
                .PrimaryKey(t => t.YazarID);
            
            CreateTable(
                "dbo.Yorum",
                c => new
                    {
                        YorumID = c.Int(nullable: false, identity: true),
                        YorumMetni = c.String(),
                        KitapID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.YorumID)
                .ForeignKey("dbo.Kitap", t => t.KitapID, cascadeDelete: true)
                .Index(t => t.KitapID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Yorum", "KitapID", "dbo.Kitap");
            DropForeignKey("dbo.Kitap", "YazarID", "dbo.Yazar");
            DropForeignKey("dbo.Kitap", "YayinEviID", "dbo.YayinEvi");
            DropIndex("dbo.Yorum", new[] { "KitapID" });
            DropIndex("dbo.Kitap", new[] { "YazarID" });
            DropIndex("dbo.Kitap", new[] { "YayinEviID" });
            DropTable("dbo.Yorum");
            DropTable("dbo.Yazar");
            DropTable("dbo.YayinEvi");
            DropTable("dbo.Kitap");
        }
    }
}
