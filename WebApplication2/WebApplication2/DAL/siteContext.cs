﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using WebApplication2.Models;

namespace WebApplication2.DAL
{
    public class siteContext : DbContext
    {
        public siteContext() : base("siteContext")
        {

        }
        public DbSet<Kitap> Kitaplar { get; set; }

        public DbSet<Yazar> Yazarlar { get; set; }

        public DbSet<Yorum> Yorumlar { get; set; }

        public DbSet<YayinEvi> YayinEvleri { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }


}