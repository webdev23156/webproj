﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.DAL;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class YayinEviController : Controller
    {
        private siteContext db = new siteContext();

        // GET: YayinEvi
        public ActionResult Index()
        {
            return View(db.YayinEvleri.ToList());
        }

        // GET: YayinEvi/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            YayinEvi yayinEvi = db.YayinEvleri.Find(id);
            if (yayinEvi == null)
            {
                return HttpNotFound();
            }
            return View(yayinEvi);
        }

        // GET: YayinEvi/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: YayinEvi/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "YayinEviID,YayinEviAdi,YayinEviHakkinda")] YayinEvi yayinEvi)
        {
            if (ModelState.IsValid)
            {
                db.YayinEvleri.Add(yayinEvi);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(yayinEvi);
        }

        // GET: YayinEvi/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            YayinEvi yayinEvi = db.YayinEvleri.Find(id);
            if (yayinEvi == null)
            {
                return HttpNotFound();
            }
            return View(yayinEvi);
        }

        // POST: YayinEvi/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "YayinEviID,YayinEviAdi,YayinEviHakkinda")] YayinEvi yayinEvi)
        {
            if (ModelState.IsValid)
            {
                db.Entry(yayinEvi).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(yayinEvi);
        }

        // GET: YayinEvi/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            YayinEvi yayinEvi = db.YayinEvleri.Find(id);
            if (yayinEvi == null)
            {
                return HttpNotFound();
            }
            return View(yayinEvi);
        }

        // POST: YayinEvi/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            YayinEvi yayinEvi = db.YayinEvleri.Find(id);
            db.YayinEvleri.Remove(yayinEvi);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
