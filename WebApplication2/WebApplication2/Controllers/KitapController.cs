﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApplication2.DAL;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class KitapController : Controller
    {
        private siteContext db = new siteContext();

        // GET: Kitap
        public ActionResult Index()
        {
            var kitaplar = db.Kitaplar.Include(k => k.YayinEvi).Include(k => k.Yazar);
            return View(kitaplar.ToList());
        }

        // GET: Kitap/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kitap kitap = db.Kitaplar.Find(id);
            if (kitap == null)
            {
                return HttpNotFound();
            }
            return View(kitap);
        }

        // GET: Kitap/Create
        public ActionResult Create()
        {
            ViewBag.YayinEviID = new SelectList(db.YayinEvleri, "YayinEviID", "YayinEviAdi");
            ViewBag.YazarID = new SelectList(db.Yazarlar, "YazarID", "YazarAd");
            return View();
        }

        // POST: Kitap/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "KitapID,KitapAd,YayinTarihi,SayfaSayisi,Tiklanma,YayinEviID,YazarID,Kategori,GenelBilgi")] Kitap kitap)
        {
            if (ModelState.IsValid)
            {
                db.Kitaplar.Add(kitap);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.YayinEviID = new SelectList(db.YayinEvleri, "YayinEviID", "YayinEviAdi", kitap.YayinEviID);
            ViewBag.YazarID = new SelectList(db.Yazarlar, "YazarID", "YazarAd", kitap.YazarID);
            return View(kitap);
        }

        // GET: Kitap/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kitap kitap = db.Kitaplar.Find(id);
            if (kitap == null)
            {
                return HttpNotFound();
            }
            ViewBag.YayinEviID = new SelectList(db.YayinEvleri, "YayinEviID", "YayinEviAdi", kitap.YayinEviID);
            ViewBag.YazarID = new SelectList(db.Yazarlar, "YazarID", "YazarAd", kitap.YazarID);
            return View(kitap);
        }

        // POST: Kitap/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "KitapID,KitapAd,YayinTarihi,SayfaSayisi,Tiklanma,YayinEviID,YazarID,Kategori,GenelBilgi")] Kitap kitap)
        {
            if (ModelState.IsValid)
            {
                db.Entry(kitap).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.YayinEviID = new SelectList(db.YayinEvleri, "YayinEviID", "YayinEviAdi", kitap.YayinEviID);
            ViewBag.YazarID = new SelectList(db.Yazarlar, "YazarID", "YazarAd", kitap.YazarID);
            return View(kitap);
        }

        // GET: Kitap/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Kitap kitap = db.Kitaplar.Find(id);
            if (kitap == null)
            {
                return HttpNotFound();
            }
            return View(kitap);
        }

        // POST: Kitap/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Kitap kitap = db.Kitaplar.Find(id);
            db.Kitaplar.Remove(kitap);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
